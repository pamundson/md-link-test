# Testing Markdown File Linking within BitBucket

This is only for repository files, not linking within wiki content.

## Example Tests:

> ## Parent URL Links
> `[Parent Link](readme.md)` (fails - navigates to current file since there is no '/' to change directories)
> 
> [Parent Link](readme.md)
> 
> ---------------------------------
> 
> `[Parent Link](/readme.md)` *(works!)*
> 
> [Parent Link](/readme.md)
> 
> ---------------------------------
> 
> `[Parent Link](./readme.md)` (fails - navigates to current file since in UNIX './' is meant to denote the current directory)
> 
> [Parent Link](./readme.md)
> 
> ---------------------------------
> 
> `[Parent Link](../readme.md)` *(works!)*
> 
> [Parent Link](../readme.md)
> 


> ## Sub-subdirectory URL Links
> `[Subsubdirectory Link](subsubdirectory/readme.md)` *(works!)*
> 
> [Subsubdirectory Link](subsubdirectory/readme.md)
> 
> ---------------------------------
> 
> `[Subsubdirectory Link](/subsubdirectory/readme.md)` (fails - the leading '/' causes the file lookup to start at the root of the repository)
> 
> [Subsubdirectory Link](/subsubdirectory/readme.md)
> 
> ---------------------------------
> 
> `[Subsubdirectory Link](./subsubdirectory/readme.md)` *(works!)*
> 
> [Subsubdirectory Link](./subsubdirectory/readme.md)
> 
> ---------------------------------
> 
> `[Subsubdirectory Link](../subsubdirectory/readme.md)` (fails - this reasonably should fail)
> 
> [Subsubdirectory Link](../subsubdirectory/readme.md)
> 
> ---------------------------------
> 
> `[Subsubdirectory Link](subdirectory/subsubdirectory/readme.md)` (fails - this reasonably should fail since this path is relative to the current directory)
> 
> [Subsubdirectory Link](subdirectory/subsubdirectory/readme.md)
> 
> ---------------------------------
> 
> `[Subsubdirectory Link](/subdirectory/subsubdirectory/readme.md)` *(works!)*
> 
> [Subsubdirectory Link](/subdirectory/subsubdirectory/readme.md)
> 
> ---------------------------------
> 
> `[Subsubdirectory Link](./subdirectory/subsubdirectory/readme.md)` (fails - in UNIX './' is meant to denote the current directory)
> 
> [Subsubdirectory Link](./subdirectory/subsubdirectory/readme.md)
> 
> ---------------------------------
> 
> `[Subsubdirectory Link](../subdirectory/subsubdirectory/readme.md)` *(works!)*
> 
> [Subsubdirectory Link](../subdirectory/subsubdirectory/readme.md)
> 


> ## Subdirectory Images
> 
> `![Subdirectory Image](images/gordians_knot.jpg)` (fails)
> 
> ![Subdirectory Image](images/gordians_knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](/images/gordians_knot.jpg)` *(works!)*
> 
> ![Subdirectory Image](/images/gordians_knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](./images/gordians_knot.jpg)` (fails)
> 
> ![Subdirectory Image](./images/gordians_knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](././images/gordians_knot.jpg)` (fails)
> 
> ![Subdirectory Image](././images/gordians_knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](../images/gordians_knot.jpg)` *(works!)*
> 
> ![Subdirectory Image](../images/gordians_knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](../../images/gordians_knot.jpg)` (fails)
> 
> ![Subdirectory Image](../../images/gordians_knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](./../images/gordians_knot.jpg)` *(works!)*
> 
> ![Subdirectory Image](./../images/gordians_knot.jpg)