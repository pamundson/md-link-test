# Testing Markdown File Linking within BitBucket

This is only for repository files, not linking within wiki content.

## Example Tests:

> ## Image Type JPG
> 
> `![Image](./images/gordiansknot.jpg)`
> 
> ![Image](./images/gordiansknot.jpg)
> 
> ---------------------------------
> 
> `![Image](./images/gordians_knot.jpg)`
> 
> ![Image](./images/gordians_knot.jpg)
> 
> ---------------------------------
> 
> `![Image](./images/gordians-knot.jpg)`
> 
> ![Image](./images/gordians-knot.jpg)
> 

> ## Image Type PNG
> 
> `![Image](./images/gordiansknot.png)`
> 
> ![Image](./images/gordiansknot.png)
> 

> ## Image Type GIF
> 
> `![Image](./images/gordiansknot.gif)`
> 
> ![Image](./images/gordiansknot.gif)
> 

> ## Image Type BMP
> 
> `![Image](./images/gordiansknot.bmp)`
> 
> ![Image](./images/gordiansknot.bmp)
> 