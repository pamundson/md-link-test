# Testing Markdown File Linking within BitBucket

This is only for repository files, not linking within wiki content.

* This is a similar approach to this guide for rst files [here](https://bitbucket.org/tlroche/rst_link_test/). _(absolute links work fine)_
* There is lots of feedback for this within this closed BitBucket [Issue](https://bitbucket.org/site/master/issues/6315/relative-urls-in-readmemd-files-only-work).
* Markdown syntax is detailed [here](http://daringfireball.net/projects/markdown/syntax).

> ## Subdirectory URL Links
> `[Subdirectory Link](subdirectory/readme.md)` *(works!)*
> 
> [Subdirectory Link](subdirectory/readme.md)
> 
> ---------------------------------
> 
> `[Subdirectory Link](/subdirectory/readme.md)` *(works!)*
> 
> [Subdirectory Link](/subdirectory/readme.md)
> 
> ---------------------------------
> 
> `[Subdirectory Link](./subdirectory/readme.md)` *(works!)*
> 
> [Subdirectory Link](./subdirectory/readme.md)
> 
> ---------------------------------
> 
> `[Subdirectory Link](../subdirectory/readme.md)` (fails - this reasonably should fail)
> 
> [Subdirectory Link](../subdirectory/readme.md)
> 

> ## Subdirectory Images
> 
> `![Subdirectory Image](images/gordians_knot.jpg)` *(works!)*
> 
> ![Subdirectory Image](images/gordians_knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](/images/gordians_knot.jpg)` *(works!)*
> 
> ![Subdirectory Image](/images/gordians_knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](./images/gordians_knot.jpg)` *(works!)*
> 
> ![Subdirectory Image](./images/gordians_knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](../images/gordians_knot.jpg)` (fails - this reasonably should fail)
> 
> ![Subdirectory Image](../images/gordians_knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](images/gordians-knot.jpg)`
> 
> ![Subdirectory Image](images/gordians-knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](/images/gordians-knot.jpg)`
> 
> ![Subdirectory Image](/images/gordians-knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](./images/gordians-knot.jpg)`
> 
> ![Subdirectory Image](./images/gordians-knot.jpg)
> 
> ---------------------------------
> 
> `![Subdirectory Image](../images/gordians-knot.jpg)`
> 
> ![Subdirectory Image](../images/gordians-knot.jpg)